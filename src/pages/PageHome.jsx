import React from "react";
/* components */
import { HeroHeader } from "../components/HeroHeader";
import { HeroVideo } from "../components/HeroVideo";
import { SectionColection } from "../components/SectionColection";
import { SectionMainStore } from "../components/SectionMainStore";
import { SectionMost } from "../components/SectionMost";
import { SectionChild } from "../components/SectionChild";
import { FooterStore } from "../components/FooterStore";

export const PageHome = () => {
  return (
    <div className="">
      <header>
        <HeroHeader />
      </header>
      <section className="">
        <HeroVideo />
      </section>
      <section className="">
        <SectionColection />
      </section>
      <main className="">
        <SectionMainStore />
      </main>
      <section className="">
        <SectionMost />
      </section>
      <section className="">
        <SectionChild />
      </section>
      <section className="">
        <FooterStore />
      </section>
    </div>
  );
};
