import React, { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
/* styles with framework tailwind */
import "./static/css/index.css";
/* libraires */
import AOS from "aos";
import "aos/dist/aos.css";
/* components pages */
import { PageHome } from "./pages/PageHome";
const App = () => {
  useEffect(() => {
    AOS.init();
    /* AOS.refresh(); */
  }, []);
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageHome />} />
          <Route path="/home" element={<PageHome />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
