import React, { useState, useRef, useEffect } from "react";
/* media img */
import ChevronLeft from "../../static/img/chevron-left.svg";

export const Slider = ({ components, numerItems = 3 }) => {
  /* vars usestate */
  const [ListProductItem, setListProductItem] = useState();
  const [CountItem, setCountItem] = useState(0);
  const [WidthContainerSlider, setWidthContainerSlider] = useState(0);
  const [PositionContainerSlider, setPositionContainerSlider] = useState(0);
  const RefProductItems = useRef();
  let screenWidth = window.innerWidth;
  /* Prev */
  const Next = () => {
    /* container complete */
    if (screenWidth > 780 && CountItem < numerItems - 1) {
      /* iter product and add class */
      ListProductItem[CountItem].classList.add("scale-100");
      ListProductItem[CountItem].classList.remove("scale-110");
      /* add class */
      ListProductItem[CountItem + 1].classList.add("scale-110");
      ListProductItem[CountItem + 1].classList.remove("scale-100");

      setPositionContainerSlider(
        PositionContainerSlider - WidthContainerSlider / numerItems
      );
      setCountItem(CountItem + 1);
    } else {
      if (CountItem < numerItems - 1) {
        console.log(CountItem);
        ListProductItem[CountItem].classList.add("hidden");
        ListProductItem[CountItem].classList.remove("block");
        /* add class */
        ListProductItem[CountItem + 1].classList.add("block");
        ListProductItem[CountItem + 1].classList.remove("hidden");
        /* condictional error counter */
        setCountItem(CountItem + 1);
      }
    }
  };

  const Prev = () => {
    if (screenWidth > 780 && CountItem >= 1) {
      /* iter product and add class */
      ListProductItem[CountItem].classList.add("scale-100");
      ListProductItem[CountItem].classList.remove("scale-110");
      /* add class */
      ListProductItem[CountItem - 1].classList.add("scale-110");
      ListProductItem[CountItem - 1].classList.remove("scale-100");

      setPositionContainerSlider(
        PositionContainerSlider + WidthContainerSlider / numerItems
      );
      /* condictional error counter */
      setCountItem(CountItem - 1);
    } else {
      if (CountItem >= 1) {
        ListProductItem[CountItem].classList.add("hidden");
        ListProductItem[CountItem].classList.remove("block");
        /* add class */
        ListProductItem[CountItem - 1].classList.add("block");
        ListProductItem[CountItem - 1].classList.remove("hidden");
        /* condictional error counter */
        setCountItem(CountItem - 1);
      }
    }
  };

  const AddStylesTailwindContainerItems = () => {
    /* add these classes by default to be able to make the slider work correctly */
    RefProductItems.current.childNodes.forEach((element, index) => {
      /* starts with the default scale if it is the incident 0 */
      index === 0
        ? element.classList.add(
            `${screenWidth > 780 ? "scale-110" : "block"}`,
            "md:w-1/5",
            "w-full",
            "transition-all",
            "ease-in-out",
            "transform"
            /* `w-[${screenWidth < 760 ? screenWidth }px]` */
          )
        : element.classList.add(
            `${screenWidth > 780 ? "block" : "hidden"}`,
            "md:w-1/5",
            "w-full",
            "transition-all",
            "ease-in-out",
            "transform"
            /* `w-[${screenWidth}px]` */
          );
    });
  };
  /* update config DOM reference to Element Slider */
  const PostLoadComponents = () => {
    setListProductItem(RefProductItems.current.childNodes);
    setWidthContainerSlider(RefProductItems.current.offsetWidth);
    setPositionContainerSlider(
      RefProductItems.current.offsetWidth / numerItems
    );
    /* add class list */
    AddStylesTailwindContainerItems();
  };

  /* post loading class node */
  useEffect(() => {
    PostLoadComponents();
  }, []);

  return (
    <div
      className={`relative h-full md:w-full flex justify-center items-center w-[${screenWidth}px]`}
    >
      <div
        ref={RefProductItems}
        className={`flex justify-around items-center transition-all md:w-full w-[${screenWidth}px]`}
        style={{
          transform: `translateX(${
            screenWidth > 780 ? PositionContainerSlider : "0"
          }px)`,
        }}
      >
        {components}
      </div>
      <div className="top-0 left-0 absolute w-full h-full flex justify-center items-center">
        <div className="md:w-1/3 w-full flex justify-between items-center h-full">
          <div onClick={() => Prev()} className="">
            <img
              id="left-move"
              src={ChevronLeft}
              alt="left icon"
              className="cursor-pointer hover:scale-110 scale-100 transition-all"
            />
          </div>
          <div onClick={() => Next()} className="">
            <img
              id="right-move"
              src={ChevronLeft}
              alt="right icon"
              className="transform rotate-180 cursor-pointer hover:scale-110 scale-100 transition-all"
            />
          </div>
        </div>
      </div>
    </div>
  );
};
