import React from "react";
/* components */
import { BtnGenericMain } from "./BTN/BtnGenericMain";
/* media img */
import CompBg3Disney from "../static/img/comp-bg3-disney.png";
import Comp21Disney from "../static/img/comp2-1-disney.png";
import Comp22Disney from "../static/img/comp2-2-disney.png";
/* icons decorations */
import Comp31Disney from "../static/img/comp3-1-disney.png";
import Comp32Disney from "../static/img/comp3-2-disney.png";
import Comp33Disney from "../static/img/comp3-3-disney.png";
import Comp34Disney from "../static/img/comp3-4-disney.png";
import Comp35Disney from "../static/img/comp3-5-disney.png";
import Comp36Disney from "../static/img/comp3-6-disney.png";
import Comp37Disney from "../static/img/comp3-7-disney.png";
import Comp38Disney from "../static/img/comp3-8-disney.png";

export const SectionMainStore = () => {
  return (
    <div className="relative mt-96">
      <div className="absolute top-0 left-0 w-full h-full">
        <div className="relative w-full h-full">
          <img
            src={Comp31Disney}
            alt=""
            className="absolute xl:w-40 md:w-32 top-10 left-10 hidden md:block"
          />
          <img
            src={Comp32Disney}
            alt=""
            className="absolute xl:w-96 md:w-40 -top-40 left-96 hidden md:block"
          />
          <img
            src={Comp33Disney}
            alt=""
            className="absolute xl:w-72 md:w-40 -top-20 right-72 hidden md:block"
          />
          <img
            src={Comp34Disney}
            alt=""
            className="absolute xl:w-40 md:w-32 -top-20 right-32 hidden md:block"
          />
          <img
            src={Comp38Disney}
            alt=""
            className="absolute xl:w-40 md:w-32 -bottom-20 right-32 hidden md:block"
          />
          <img
            src={Comp35Disney}
            alt=""
            className="absolute xl:w-40 md:w-32 -bottom-20 left-10 hidden md:block"
          />
          <img
            src={Comp37Disney}
            alt=""
            className="absolute xl:w-40 md:w-32 bottom-5 right-40 hidden md:block"
          />
          <img
            src={Comp36Disney}
            alt=""
            className="absolute xl:w-40 md:w-32 -bottom-10 left-96 z-50 hidden md:block"
          />
        </div>
      </div>
      <div className="absolute left-0 top-0 w-full -z-50 md:h-auto">
        <img src={CompBg3Disney} alt="" className="w-full h-[45rem]" />
      </div>
      <div className="flex md:flex-row flex-col justify-between items-center w-9/12 mx-auto pt-32">
        <div
          className="md:w-1/2 w-9/12 md:mr-32 mr-0 md:mb-0 mb-24 relative"
          data-aos="fade-up"
          data-aos-delay="300"
        >
          <img src={Comp21Disney} alt="" className="w-full" />
          <div className="absolute w-full flex justify-center -bottom-8">
            <BtnGenericMain value="comprar ahora" />
          </div>
        </div>
        <div
          className="md:w-1/2 w-9/12"
          data-aos="fade-up"
          data-aos-delay="300"
        >
          <img src={Comp22Disney} alt="" className="w-full" />
        </div>
      </div>
    </div>
  );
};
