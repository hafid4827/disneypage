import React from "react";
/* components */
import { Slider } from "./Slider/Slider";
import { BtnGenericMain } from "./BTN/BtnGenericMain";
/* media img */
import Comp61Disney from "../static/img/comp6-1-disney.png";
import Comp62Disney from "../static/img/comp6-2-disney.png";
import Comp63Disney from "../static/img/comp6-3-disney.png";
import Comp64Disney from "../static/img/comp6-4-disney.png";

export const FooterStore = () => {
  /* items img */
  const ItemslistImg = () => {
    return (
      <>
        <img src={Comp61Disney} alt="" />
        <img src={Comp62Disney} alt="" />
        <img src={Comp63Disney} alt="" />
        <img src={Comp64Disney} alt="" />
      </>
    );
  };
  return (
    <div className="xl:mt-46 mt-36">
      <div className="mt-20 mb-56 flex justify-center items-center w-full py-20 bg-[color:var(--color-pink-light)]">
        <p className="text-3xl text-gray-500 text-center w-6/12 mx-auto">
          Good vides Only with Daisy, Me and mini Me.
        </p>
      </div>
      <div className="">
        <Slider components={<ItemslistImg />} numerItems={4} />
      </div>
      <div className="my-36 w-full flex justify-center items-center">
        <BtnGenericMain value="ver coleccion" />
      </div>
    </div>
  );
};
