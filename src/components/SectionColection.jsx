import React, { useState } from "react";
/* components */
import { Slider } from "./Slider/Slider";
import { BtnGenericMain } from "./BTN/BtnGenericMain";
/* media img */
import Comp11Disney from "../static/img/comp1-1-disney.png";
import Comp12Disney from "../static/img/comp1-2-disney.png";
import Comp13Disney from "../static/img/comp1-3-disney.png";
/* media icons decoration and animation */
import CompAnime11Disney from "../static/img/comp-anime-1-1-disney.svg";
import CompAnime12Disney from "../static/img/comp-anime-1-2-disney.svg";
import CompAnime13Disney from "../static/img/comp-anime-1-3-disney.svg";
import CompAnime14Disney from "../static/img/comp-anime-1-4-disney.svg";
import CompAnime15Disney from "../static/img/comp-anime-1-5-disney.svg";

export const SectionColection = () => {
  /* vars states and static*/
  const [DirectionAnimationCounter, setDirectionAnimationCounter] = useState(1);
  const [DirectionAnimationToggle, setDirectionAnimationToggle] =
    useState(false);

  /* animation */
  const AnimaitonIconsImage = (e) => {
    /* In this case, the event is heard, 
    identify its id of the corresponding icon, 
    I capture its value and finally I extract it from the slider 
    to be able to create an event layer and thus use it with the animation. */
    let direction = e.target.id;
    console.log(direction);
    if (direction === "right-move") {
      setDirectionAnimationCounter(
        DirectionAnimationCounter <= 2
          ? DirectionAnimationCounter + 1
          : DirectionAnimationCounter
      );
      setDirectionAnimationToggle(DirectionAnimationCounter === 1);
    } else if (direction === "left-move") {
      setDirectionAnimationCounter(
        DirectionAnimationCounter >= 2
          ? DirectionAnimationCounter - 1
          : DirectionAnimationCounter
      );
      setDirectionAnimationToggle(DirectionAnimationCounter === 3);
      console.log(DirectionAnimationCounter);
    }
  };
  /* target list for component slider */
  /* error render event animation figures */
  /* const Tagets = () => {
    return (
      <>
        <img className="" src={Comp11Disney} alt="" />
        <img className="" src={Comp12Disney} alt="" />
        <img className="" src={Comp13Disney} alt="" />
      </>
    );
  }; */
  /* use slider component and create sectio three */
  return (
    <div className="h-[80vh] relative">
      <div className="absolute top-0 left-0 md:h-1/2 h-full w-full bg-[color:var(--color-purple-generic)]"></div>
      {/* icons decoration filter */}
      <div className="absolute top-0 left-0 h-[80vh] w-full md:block hidden">
        <div className="h-full w-full flex items-center justify-center">
          <div
            className={`w-full h-1/2 relative ${
              !DirectionAnimationToggle ? "h-full w-full" : "md:w-1/3"
            }`}
          >
            <img
              src={CompAnime11Disney}
              alt=""
              className={`z-50 absolute xl:w-16 md:w-10 transition-all ${
                DirectionAnimationToggle //change state from true to false
                  ? "xl:-top-[5rem] xl:left-56 md:-top-[1rem] md:left-40"
                  : "top-10 left-20"
              }`}
            />
            <img
              src={CompAnime12Disney}
              alt=""
              className={`z-50 absolute xl:w-16 md:w-10 transition-all ${
                DirectionAnimationToggle //change state from true to false
                  ? "xl:-top-[5rem] xl:right-44 md:-top-0 md:right-32"
                  : "top-20 right-10"
              }`}
            />
            <img
              src={CompAnime13Disney}
              alt=""
              className={`z-50 absolute xl:w-16 md:w-10 transition-all ${
                DirectionAnimationToggle //change state from true to false
                  ? "xl:top-[8rem] xl:left-32 md:top-[8rem] md:left-20"
                  : "bottom-48 left-48"
              }`}
            />
            <img
              src={CompAnime14Disney}
              alt=""
              className={`z-50 absolute xl:w-16 md:w-10 transition-all ${
                DirectionAnimationToggle //change state from true to false
                  ? "xl:top-[6rem] xl:left-40 md:top-[7rem] md:left-24"
                  : "bottom-56 left-72"
              }`}
            />
            <img
              src={CompAnime15Disney}
              alt=""
              className={`z-50 absolute xl:w-24 md:w-16 transition-all ${
                DirectionAnimationToggle //change state from true to false
                  ? "xl:top-[10rem] xl:right-[9rem] md:top-[9rem] md:right-24"
                  : "bottom-36 right-10"
              }`}
            />
          </div>
        </div>
      </div>
      {/* slider container */}
      <div
        onClick={(e) => AnimaitonIconsImage(e)}
        className="h-[80vh] flex items-center justify-center"
      >
        {/* <Slider components={<Tagets />} /> */}
        <Slider
          components={
            <>
              <img className="" src={Comp11Disney} alt="" />
              <img className="" src={Comp12Disney} alt="" />
              <img className="" src={Comp13Disney} alt="" />
            </>
          }
        />
      </div>
      {/* info container */}
      <div className="py-20 relative bg-[color:var(--color-pink-light)]">
        <p className="w-9/12 mx-auto text-2xl text-gray-500 text-center">
          La mas coqueta de Disney llega a Ela y Ela Kids. La pata Daisy esta
          aqui para llenar tus dias de diversion y magia.
        </p>
        <div className="absolute -bottom-8 left-0 w-full flex justify-center">
          <BtnGenericMain value="ver coleccion" />
        </div>
      </div>
    </div>
  );
};
