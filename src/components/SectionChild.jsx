import React from "react";
/* media img */
import comp51Disney from "../static/img/comp5-1-disney.png";
import comp52Disney from "../static/img/comp5-2-disney.png";
import comp53Disney from "../static/img/comp5-3-disney.png";

export const SectionChild = () => {
  return (
    <div className="mt-32 md:h-screen h-[250vh] w-full flex justify-center items-center relative">
      <div className="absolute top-0 right-0 md:w-8/12 w-9/12 h-2/5 -z-50 bg-[color:var(--color-yellow-light)] rounded-3xl"></div>
      <div className="absolute bottom-0 left-0 md:w-2/5 w-9/12 h-1/2 -z-50 bg-[color:var(--color-pink-light)] rounded-3xl"></div>
      <div className="flex md:flex-row flex-col justify-between items-center mx-auto w-9/12 md:h-screen h-full md:p-0 py-10">
        <div className="md:mb-0 mb-10">
          <img
            src={comp51Disney}
            alt=""
            className=""
            data-aos="fade-up"
            data-aos-delay="300"
          />
        </div>
        <div className="md:mx-10 mx-0 md:mb-0 mb-10">
          <img
            src={comp52Disney}
            alt=""
            className=""
            data-aos="fade-up"
            data-aos-delay="350"
          />
        </div>
        <div className="md:mb-0 mb-10">
          <img
            src={comp53Disney}
            alt=""
            className=""
            data-aos="fade-up"
            data-aos-delay="400"
          />
        </div>
      </div>
    </div>
  );
};
