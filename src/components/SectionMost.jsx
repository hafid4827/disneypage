import React, { useState } from "react";

/* media img */
import Comp41Disney from "../static/img/comp4-1-disney.png";
import Comp42Disney from "../static/img/comp4-2-disney.png";
import ChevronLeft from "../static/img/chevron-left.svg";
/* img icon decoration */
import CompAnime21Disney from "../static/img/comp-anime-2-1-disney.svg";
import CompAnime22Disney from "../static/img/comp-anime-2-2-disney.svg";
import CompAnime23Disney from "../static/img/comp-anime-2-3-disney.svg";
import CompAnime24Disney from "../static/img/comp-anime-2-4-disney.svg";
import CompAnime25Disney from "../static/img/comp-anime-2-5-disney.svg";
import CompAnime26Disney from "../static/img/comp-anime-2-6-disney.svg";
import CompAnime27Disney from "../static/img/comp-anime-2-7-disney.svg";
import CompAnime28Disney from "../static/img/comp-anime-2-8-disney.svg";

export const SectionMost = () => {
  const [tradeImage, setTradeImage] = useState(false);
  return (
    <div className="relative xl:mt-56 md:mt-20 mt-48">
      <div className="absolute w-full h-full top-0 left-0 md:block hidden">
        <div className="relative h-full w-full">
          <img
            src={CompAnime21Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:top-10 xl:left-[26rem] md:left-52 md:top-40 "
                : "xl:bottom-10 xl:left-20 md:bottom-10 md:left-20"
            }`}
          />
          <img
            src={CompAnime22Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:top-16 xl:left-[38rem] md:top-28 md:left-72"
                : "right-[50%] xl:-top-20"
            }`}
          />
          <img
            src={CompAnime23Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:top-60 xl:left-[41rem] md:top-64 md:left-[21.5rem]"
                : "xl:left-20 xl:bottom-20"
            }`}
          />
          <img
            src={CompAnime24Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:top-96 xl:left-[45rem] md:top-96 md:left-96"
                : "hidden"
            }`}
          />
          <img
            src={CompAnime25Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:bottom-28 xl:left-[43rem] md:top-80 left-[22.5rem]"
                : "xl:-bottom-40 xl:left-[55rem]"
            }`}
          />
          <img
            src={CompAnime27Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:top-96 xl:left-[22rem] md:top-40 md:left-36"
                : "xl:top-0 xl:left-20 "
            }`}
          />
          <img
            src={CompAnime26Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:top-72 xl:left-[20rem] md:bottom-80 md:left-40"
                : "hidden"
            }`}
          />
          <img
            src={CompAnime28Disney}
            alt=""
            className={`z-50 w-32 md:w-20 absolute transition-all ${
              !tradeImage
                ? "xl:top-[35rem] xl:left-[23rem] md:bottom-52 md:left-44"
                : "hidden"
            }`}
          />
        </div>
      </div>
      <div className="md:h-screen h-auto w-9/12 mx-auto">
        <div className="flex justify-center items-center">
          <div className="flex md:flex-row flex-col md:justify-between justify-around items-center md:h-screen h-auto">
            <div className="md:w-1/3 w-9/12 mx-auto md:mb-0 mb-36">
              <img
                src={Comp41Disney}
                alt=""
                className={`w-full transform transition-all ${
                  !tradeImage ? "scale-125" : "scale-100"
                } `}
              />
            </div>
            {/* rotate icon options */}
            <div
              onClick={() => setTradeImage(!tradeImage)}
              className="md:block hidden cursor-pointer hover:scale-110 scale-100 transition-all"
            >
              <span
                className={`inline-block transition-all transform ${
                  tradeImage ? "rotate-180" : "-rotate-0"
                }`}
              >
                <img src={ChevronLeft} alt="" />
              </span>
            </div>
            <div className="md:w-1/3 w-9/12 mx-auto">
              <img
                src={Comp42Disney}
                alt=""
                className={`w-full transform transition-all ${
                  !tradeImage
                    ? "md:scale-100 scale-125"
                    : "md:scale-125 scale-100"
                }`}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="-z-50 absolute bottom-0 left-0  md:h-[50vh] h-full w-full bg-[color:var(--color-purple-generic)]"></div>
    </div>
  );
};
