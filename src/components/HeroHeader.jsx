import React from "react";
/* media img */
import Comp0Disney from "../static/img/comp0-disney.png";
export const HeroHeader = () => {
  return (
    <div className="h-screen w-full flex items-center justify-center">
      <div className="wobble md:p-0 p-5">
        <img
          className="md:w-96 w-full"
          src={Comp0Disney}
          alt="logo hero header"
        />
      </div>
    </div>
  );
};
