import React from "react";
/* media video */
import Bgvideo from "../static/video/BgMain.mp4";
export const HeroVideo = () => {
  return (
    <div className="md:my-0 my-20">
      <video
        className="w-full"
        src={Bgvideo}
        autoPlay="true"
        muted
        controls
        loop
      />
    </div>
  );
};
