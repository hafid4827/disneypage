import React from "react";

export const BtnGenericMain = ({ value }) => {
  return (
    <button className="capitalize rounded-full py-3 px-5 text-center text-2xl font-semibold hover:scale-110 scale-100 transition-all transform mx-auto text-white bg-[color:var(--color-purple-generic)]">
      {value}
    </button>
  );
};
